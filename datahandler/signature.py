# -*- coding: utf-8 -*-
"""
Représentation graphique des signatures spectrale et temporelles

@author: thomas_a
"""
from __future__ import unicode_literals
import logging

import numpy as np
import matplotlib.pyplot as plt

#from matplotlib.ticker import MaxNLocator
#from matplotlib.dates import date2num, DateFormatter
from matplotlib.dates import date2num
# Turn interactive plotting off
plt.ioff()
logger = logging.getLogger('maplearn.' + __name__)


class Signature(object):
    """
    Représentation graphique des signatures d'un jeu de données ou de classes
    """

    def __init__(self, dates=None):
        self.dates = dates

    def plot(self, data, title, label='', features=None, out_file=None):
        """
        Affichage graphique synthétique des profils
        Entrées :
            - data = matrice avec les données MODIS
            - aGr = vecteur traduisant les groupes
            - out_file : nom de fichier en sortie
            - labels : nom de la variable en Y
            - y_lim [min, max] : limites en Y du graphique

        """
        logger.debug('Préparation du graphique...')
        if not isinstance(data, np.ndarray):
            data = np.array(data)
        profil = {'min': np.nanmin(data, axis=0),
                  'max': np.nanmax(data, axis=0),
                  'med': np.median(data, axis=0)}

        fig = plt.figure(figsize=(7, 5))
        ax0 = fig.add_subplot(1, 1, 1)

        # limites en ordonnée comprises entre 0 et 100
        if not self.dates is None:
            ax0.set_xlim([self.dates[0], self.dates[-1]])
        if label != '':
            ax0.set_ylabel(label)
        if not features is None:
            ax0.set_xlabel(features)

        if not self.dates is None:
            xlab = date2num(self.dates)
            fig.autofmt_xdate()
        else:
            xlab = range(1, data.shape[1] + 1)

        # Ajout des lignes graphiques (plot de chaque ligne)
        line, = ax0.plot(xlab, profil['med'], 'o--', color='0.5')
        line, = ax0.plot(xlab, profil['min'], '--', color='0.75')
        line, = ax0.plot(xlab, profil['max'], '--', color='0.75')

        ax0.set_title(title)

        if not out_file is None:
            plt.savefig(out_file, format='png', bbox_inches='tight')
            logger.info(u'Graphique enregistré (%s)', out_file)

        plt.close(fig)
        fig = None

    def __color_box(self, boxplot, zorder, alpha):
        """
        Colorie les boîtes à Moustaches du boxplot
        """
        for box in boxplot['boxes']:
            # change outline color & fill color
            box.set(color='darkgrey', linewidth=2, facecolor='lightgrey',
                    zorder=zorder + 1, alpha=alpha)

        # change color and linewidth of the whiskers
        for whisker in boxplot['whiskers']:
            whisker.set(color='darkgrey', linewidth=2, linestyle='-',
                        zorder=zorder + 2, alpha=alpha)

        # change color and linewidth of the caps
        for cap in boxplot['caps']:
            cap.set(color='darkgrey', linewidth=2, linestyle='-',
                    zorder=zorder + 3, alpha=alpha)

        # change color and linewidth of the medians
        for median in boxplot['medians']:
            median.set(color='darkgrey', linewidth=3, linestyle='-',
                       zorder=zorder + 4, alpha=alpha)

        # fliers <=> outliers
        plt.setp(boxplot['fliers'], color='white', marker='o', markersize=5.0,
                 zorder=zorder + 5, alpha=alpha)

    def boxplot(self, data, title='Signature', features=None, out_file=None):
        """
        Création de la signature sous la forme de boîte à moustaches
        => pour les données non temporelles
        """
        logger.debug('Préparation du graphique...')
        fig, ax1 = plt.subplots(nrows=1, ncols=1)  # , figsize=(7, 5)

        # Add a horizontal grid to the plot, but make it very light in color
        # so we can use it for reading data values but not be distracting
        ax1.yaxis.grid(True, linestyle='-', which='major', color='grey',
                       alpha=0.5, zorder=0)

        bp = ax1.boxplot(data, notch=1, patch_artist=True, vert=1,
                         whis=1.3)
        self.__color_box(bp, zorder=0, alpha=1)

        ax1.set_title(title)
        if not features is None:
            # TODO : ajouter des vérifications
            ax1.set_xticklabels(features)

        if not out_file is None:
            plt.savefig(out_file, bbox_inches='tight')
            logger.info(u'Graphique enregistré (%s)', out_file)

        plt.close(fig)
        fig = None

    def boxplot_classe(self, data, data_classe, title='', features=None,
                       out_file=None):
        """
        Création de la signature sous la forme de boîte à moustaches
        => pour les données non temporelles
        """
        logger.debug('Préparation du graphique...')

        positions = [i for i in range(1, data.shape[1] + 1)]
        fig, ax1 = plt.subplots(figsize=(7, 5))

        # Add a horizontal grid to the plot, but make it very light in color
        # so we can use it for reading data values but not be distracting
        ax1.yaxis.grid(True, linestyle='-', which='major', color='grey',
                       alpha=0.5, zorder=0)

        bp = plt.boxplot(data, notch=1, patch_artist=True, vert=1,
                         whis=1.3, positions=[i - .1 for i in positions])

        self.__color_box(bp, zorder=0, alpha=0.5)

        plt.hold(True)  # superposition d'un second graphe
        bp = plt.boxplot(data_classe, notch=1, patch_artist=True, vert=1,
                         whis=1.3, positions=[i for i in positions])

        self.__color_box(bp, zorder=6, alpha=1)

        ax1.set_title(title)
        if not features is None:
            # TODO : ajouter des vérifications
            ax1.set_xticklabels(features)
        else:
            ax1.set_xticklabels(positions)

        if not out_file is None:
            try:
                plt.savefig(out_file, format='png', bbox_inches='tight')
            except IOError as e:
                logger.error("Missing folder : %s", out_file)
                raise IOError(e.message)
            else:
                logger.info(u'Graphique enregistré (%s)', out_file)

        # plt.show()
        plt.close(fig)
        fig = None
