# -*- coding: utf-8 -*-
"""
Created on Tue Aug 16 18:57:34 2016

@author: thomas_a
"""
import os
import unittest
import random

from maplearn.test import DIR_DATA
from maplearn.datahandler.loader import Loader
from maplearn.filehandler.shapefile import Shapefile
from maplearn.filehandler.imagegeo import ImageGeo


class TestLoader(unittest.TestCase):
    """
    Test de chargement de fichier
    """

    def test_fichier_inexistant(self):
        """
        Tente de charge un fichier inexistant
        """
        self.assertRaises(IOError, Loader, '/fichier/inexistant')

    def test_dataset(self):
        """
        Tente de charger un jeu de données fourni dans Scikit
        """
        data = random.choice(['boston', 'iris', 'digits'])
        loader = Loader(data)
        test = loader.X is None or loader.Y is None
        self.assertFalse(test)

    def test_extension_inconnu(self):
        """
        Tente de charger des données à partir d'extensions non gérées
        """
        self.assertRaises(IOError, Loader, '/fichier/inexistant.shp.xml')

    def test_shapefile(self):
        """
        Compare le chargement d'1 shapefile avec la classe
        dédiée et le résultat de la classe loader
        """
        src = os.path.join(DIR_DATA, 'echantillon.shp')
        shp = Shapefile(src)
        shp.read()
        loader = Loader(src, classe='ECH')
        self.assertEqual(loader.aData.shape[0], shp.data.shape[0])
        self.assertEqual(loader.aData.shape[1], shp.data.shape[1] - 1)

    def test_image(self):
        """
        Compare le chargement d'1 image avec la classe
        dédiée et le résultat de la classe loader
        """
        src = os.path.join(DIR_DATA, 'landsat_rennes.tif')
        img = ImageGeo(src)
        img.read_geo()
        loader = Loader(src)
        self.assertEqual(loader.aData.shape[0],
                         img.data.shape[0] * img.data.shape[1])

if __name__ == '__main__':
    unittest.main()
